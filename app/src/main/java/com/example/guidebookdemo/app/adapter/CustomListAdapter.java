package com.example.guidebookdemo.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.guidebookdemo.app.R;
import com.example.guidebookdemo.app.model.Data;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class CustomListAdapter extends ArrayAdapter<Data> {
    private Context mContext;
    private List<Data> mUpcomingGuidesList;

    public CustomListAdapter(Context context, int resource, List<Data> objects) {
        super(context, resource, objects);

        this.mContext = context;
        this.mUpcomingGuidesList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        View rowView = convertView;

        if (convertView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(R.layout.list_row, null);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) rowView.findViewById(R.id.content);
            viewHolder.startDate = (TextView) rowView.findViewById(R.id.startDate);
            viewHolder.endDate = (TextView) rowView.findViewById(R.id.endDate);
            viewHolder.name = (TextView) rowView.findViewById(R.id.content);
            viewHolder.city = (TextView) rowView.findViewById(R.id.city);
            viewHolder.state = (TextView) rowView.findViewById(R.id.state);
            viewHolder.icon = (ImageView) rowView.findViewById(R.id.icon);

            rowView.setTag(viewHolder);
        }

        viewHolder = (ViewHolder) rowView.getTag();

        DateFormat df = new SimpleDateFormat("MMM dd, yyyy");
        String aDate = df.format(mUpcomingGuidesList.get(position).getStartDate());
        viewHolder.startDate.setText(aDate == null || aDate.isEmpty() ? "N/A" : aDate);

        aDate = df.format(mUpcomingGuidesList.get(position).getEndDate());
        viewHolder.endDate.setText(aDate == null || aDate.isEmpty()  ? "N/A" : aDate);

        String name = mUpcomingGuidesList.get(position).getName();
        viewHolder.name.setText(name == null || name.isEmpty() ? "N/A" : name);

        String city = mUpcomingGuidesList.get(position).getVenue().getCity();
        viewHolder.city.setText(city == null || city.isEmpty() ? "N/A, " : city + ", ");

        String state = mUpcomingGuidesList.get(position).getVenue().getState();
        viewHolder.state.setText(state == null || state.isEmpty() ? "N/A" : state);

        // Load and bind to imageview
        Picasso.with(mContext).load(mUpcomingGuidesList.get(position).getIcon()).into(viewHolder.icon);

        return rowView;
    }

    private static class ViewHolder {
        TextView startDate;
        TextView endDate;
        TextView name;
        TextView city;
        TextView state;
        ImageView icon;
    }
}


