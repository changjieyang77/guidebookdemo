package com.example.guidebookdemo.app.model;

/**
 * Created by ljxi_828 on 3/12/14.
 */
public class Venue {
    private static final long serialVersionUID = 3L;
    private String city;
    private String state;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
