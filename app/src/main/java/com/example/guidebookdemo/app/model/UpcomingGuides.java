package com.example.guidebookdemo.app.model;

import java.util.List;

/**
 * Created by ljxi_828 on 3/12/14.
 */
public class UpcomingGuides {
    private static final long serialVersionUID = 1L;

    private int total;
    private List<Data> data;


    public UpcomingGuides(){}

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }
}
