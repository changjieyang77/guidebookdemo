package com.example.guidebookdemo.app.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Http manager
 */
public class HttpManager {

    // Use HttpURLConnection
    public static String doGetHttpURLConnection(String aUrl) {
        final String TAG = HttpManager.class.getSimpleName();

        StringBuilder stringBuilder = new StringBuilder();

        try {
            URL url = new URL(aUrl);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestMethod("GET");

            int httpStatus = conn.getResponseCode();
            Log.v(TAG, "httpStatus " + httpStatus);

            if (httpStatus == 200) {

                //BufferedInputStream bufferedInputStream = new BufferedInputStream(conn.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                Log.d(TAG, stringBuilder.toString());

            }
        } catch (MalformedURLException me) {
            Log.d(TAG, me.getMessage());
        } catch (IOException ioe) {
            Log.d(TAG, ioe.getMessage());
        }

        return stringBuilder.toString();
    }

    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;

    }
}
