package com.example.guidebookdemo.app.model;

import java.util.Date;

/**
 * Created by ljxi_828 on 3/12/14.
 */
public class Data {
    private static final long serialVersionUID = 2L;

    private Date startDate;
    private Date endDate;
    private String name;
    private String url;
    private Venue venue;
    private String icon;

    public Data() {
    }


    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
