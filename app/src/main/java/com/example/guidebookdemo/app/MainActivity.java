package com.example.guidebookdemo.app;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.example.guidebookdemo.app.adapter.CustomListAdapter;
import com.example.guidebookdemo.app.fragment.RetainFragment;
import com.example.guidebookdemo.app.model.Data;
import com.example.guidebookdemo.app.model.UpcomingGuides;
import com.example.guidebookdemo.app.network.HttpManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ActionBarActivity {
    private String TAG = MainActivity.class.getSimpleName();

    private Context mContext;
    private ListView mListView;
    private CustomListAdapter mCustomListAdapter;
    private List<Data> mUpcomingGuidesList = new ArrayList<Data>();
    private String mUpcomingGuides;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup listview section
        mListView = (ListView) findViewById(R.id.listview);
        mCustomListAdapter = new CustomListAdapter(this,
                R.layout.list_row, mUpcomingGuidesList);
        mListView.setAdapter(mCustomListAdapter);
        mListView.setEmptyView(findViewById(R.id.empty));


        if (!HttpManager.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, "Network Error.",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        // Save the data object to the retain fragment to handle orientation change
        final RetainFragment mRetainFragment = RetainFragment.findOrCreateRetainFragment(this
                .getFragmentManager());

        List<Data> dataList = (List<Data>) mRetainFragment.getObject();

        if (dataList == null)
            prepareData();
        else {
            mUpcomingGuidesList = dataList;
            mCustomListAdapter.clear();
            mCustomListAdapter.addAll(mUpcomingGuidesList);
        }
    }

    private void prepareData() {

        // Execute the asyn task using background thread
        UpcomingGuidesAsyncTask upcomingGuidesAsyncTask = new UpcomingGuidesAsyncTask();
        upcomingGuidesAsyncTask.execute();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Save the data object to the retain fragment to handle orientation change
        final RetainFragment mRetainFragment = RetainFragment.findOrCreateRetainFragment(this
                .getFragmentManager());

        mRetainFragment.setObject(mUpcomingGuidesList);
    }

    // AsyncTask<Void, Void, Bitmap>: First param - input, second - progress update, third doInBackground return type
    private class UpcomingGuidesAsyncTask extends AsyncTask<Void, Integer, UpcomingGuides> {

        @Override
        protected UpcomingGuides doInBackground(Void... params) {
            UpcomingGuides upcomingGuides = null;

            try {
                // Fetch data from the network
                mUpcomingGuides = HttpManager.doGetHttpURLConnection(Constant.URL);
                Log.d("Main Activity", mUpcomingGuides);

                // Use Gson to convert json string to our object
                Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
                upcomingGuides = gson.fromJson(mUpcomingGuides, UpcomingGuides.class);

            } catch (Exception e) {
                Log.e(TAG, e.getLocalizedMessage(), e);
            }
            return upcomingGuides;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(UpcomingGuides upcomingGuides) {
            super.onPostExecute(upcomingGuides);

            if (upcomingGuides == null) {
                Toast.makeText(MainActivity.this, "No data found.", Toast.LENGTH_LONG).show();
                return;
            }

            // Update list data
//            mUpcomingGuidesList.clear();
//            mUpcomingGuidesList.addAll(upcomingGuides.getData());

            mCustomListAdapter.clear();
            mCustomListAdapter.addAll(upcomingGuides.getData());
            // Notify data change
            mCustomListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            prepareData();
        }
        return true;
        // return super.onOptionsItemSelected(item);
    }

}
